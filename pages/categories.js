import { useEffect, useState, useRef } from 'react'
import Head from 'next/head'
import { Table, Alert, Row, Col, Button, Container } from 'react-bootstrap'
import moment from 'moment'


export default function history(){

	const [categories, setCategories] = useState([])

  useEffect(() => {
        console.log(localStorage.getItem('token'))
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-categories`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: undefined,      
                })
            })
            .then(res => res.json())
            .then(data => {
            	console.log(data)
                setCategories(data)
            })
  }, []);

	return(
		<React.Fragment>
			<Head>
				<title>My Travel Records</title>
			</Head>
			<Container className="mt-5">
			<h2 className="mb-3">Categories</h2>
			<Button variant="primary" type="submit" href="/addCategory">Add Categories</Button>
			<Row>
				<Col className="mt-3" xs={12} lg={12}>
					{categories.length > 0
					? 
					<Table striped bordered hover>
						<thead>
                            <tr>
                                <th>Category</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                        <tbody>
                        {categories.map(category => {
                        		return(
                        			<tr key={category._id}>
                        				<td>{category.name}</td>
                        				<td>{category.type}</td>
                        			</tr> 
                                  )
                        	})}                                                  	
                        </tbody>
					</Table>
					: <Alert variant="info" className="mt-5 text-center">You have no Categories yet.</Alert>
					}
				</Col>
			</Row>
			</Container>
		</React.Fragment>
	)
}
