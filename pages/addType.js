import { useState } from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router';

export default function create() {
    //declare form input states
    const [typeName, setTypeName] = useState('');

    //function for processing creation of a new course
    // function createCourse(e) {
    //     e.preventDefault();

    //     console.log(`${courseName} with ID: ${courseId} is set to start on ${startDate} for PhP ${price} per slot.`);

    //     setCourseId('');
    //     setCourseName('');
    //     setDescription('');
    //     setPrice(0);
    //     setStartDate('');
    //     setEndDate('');
    // }


    //function to register user
        function addType(e) {
            e.preventDefault();
            console.log(`Typename: ${typeName}`)

            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/add-type`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    type: typeName,        
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data === true){
                    Router.push('/history')
                }else{
                    Router.push('/error')
                }
            })
        }



    return (
        <Container>
            <Form onSubmit={(e) => addType(e)}>
                <Form.Group controlId="courseId">
                    <Form.Label>Type Name:</Form.Label>
                    <Form.Control type="text" placeholder="Enter Type Name" value={typeName} onChange={e => setTypeName(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit">Submit</Button>
            </Form>
        </Container>
    )
}
