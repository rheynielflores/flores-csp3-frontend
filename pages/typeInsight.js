import { useEffect, useState } from 'react'
import moment from 'moment'
import Head from 'next/head'
import { Tabs, Tab, Container } from 'react-bootstrap'
import MonthlyChart from '../components/MonthlyChart'

export default function insights(){
	const [allIncome, setAllIncome] = useState([])
	const [allExpense, setAllExpense] = useState([])	



//GETTING THE DATA OF INCOME PER MONTH
useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-expenses`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: 'Income',      
                })
            })
		.then(res => res.json())
		.then(data => {
			if(data.length > 0){
				let monthlyIncome= [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


				data.forEach(Income => {
					console.log(moment(Income.dateAdded).month())
					const index = moment(Income.dateAdded).month()

					monthlyIncome[index] += (parseInt(Income.amount))
				})

				setAllIncome(monthlyIncome)
			}
		})
	}, [])

useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-expenses`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: 'Expense',      
                })
            })
		.then(res => res.json())
		.then(data => {
			if(data.length > 0){
				let monthlyExpense= [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


				data.forEach(Expense => {
					console.log(moment(Expense.dateAdded).month())
					const index = moment(Expense.dateAdded).month()

					monthlyExpense[index] += (parseInt(Expense.amount))
				})

				setAllExpense(monthlyExpense)
			}
		})
	}, [])


	return(
		<React.Fragment>
			<Head>
				<title>Bar Chart</title>
			</Head>		
	<Container className="mt-5">
		<Tabs defaultActiveKey="income" id="monthlyFigures">
			<Tab eventKey="income" title="Monthly Income">
				<MonthlyChart figuresArray={allIncome} label={"All Income"} />
			</Tab>
			<Tab eventKey="expenses" title="Monthly Expense">
				<MonthlyChart figuresArray={allExpense} label={"All Expense"} />
			</Tab>
		</Tabs>
	</Container>
	    </React.Fragment>	
	)
}
