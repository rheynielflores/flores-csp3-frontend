import { useEffect, useState, useRef } from 'react'
import Head from 'next/head'
import { Table, Alert, Row, Col, Button, Container, Badge, Form } from 'react-bootstrap'
import moment from 'moment'
import NumberFormat from 'react-number-format';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandHoldingUsd, faMinusSquare, faWallet } from '@fortawesome/free-solid-svg-icons'
export default function history(){

	const [records, setRecords] = useState([])
	const [type, setType] = useState([])
	const [categories, setCategories] = useState([])
	const [btn, setBtn] = useState('')
	const [sign, setsign] = useState('')	
	const [allIncome, setAllIncome] = useState('')
	const [allExpense, setAllExpense] = useState('')	
	const [runningBalance, setRunningBalance] = useState('')
	const [allRecords, setAllRecords] = useState([])
	const transactionsCount = allRecords.length
	const [transactTitle, setTransactTitle] = useState('Last 10 Transactions')
  const [search, setSearch] = useState('')  
  const [searchType, setSearchType] = useState('All')  
  const searchCount = records.length  



//LAST 10 TRANSACTIONS
  useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-most-recent-records`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
            	  console.log(data)
            	  setAllRecords(data)
                setRecords(data.slice(0, 10))
                console.log(data.length)
                // const total = parseInt(data.reduce((prev,next) => prev + next.amount,0));
                // setAllIncome(total)
            })
  }, []);

//Get TOTAL EXPENSE
    useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-expenses`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: 'Expense',      
                })
            })
            .then(res => res.json())
            .then(data => {
               	console.log(data)
                const total = parseInt(data.reduce((prev,next) => prev + next.amount,0));
                console.log(total)
                setAllExpense(total)   

            })
  }, []);

//Get TOTAL INCOME
    useEffect(() => {
        console.log(localStorage.getItem('token'))
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-expenses`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: 'Income',      
                })
            })
            .then(res => res.json())
            .then(data => {
               	console.log(data)
                const total = parseInt(data.reduce((prev,next) => prev + next.amount,0));
                console.log(total)
                setAllIncome(total)   

            })
  }, []);    

//Get RUNNING BALANCE
    useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-expenses`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: undefined,      
                })
            })
            .then(res => res.json())
            .then(data => {
	            	console.log(data)
	            	console.log(data.length)
	            	if(data.length >= 1){
	            	const latestBal = parseInt(data.length - 1)
	               	console.log(data[latestBal].balanceAfterTransaction)
	                setRunningBalance(data[latestBal].balanceAfterTransaction)            	
            	}else{
            		console.log("Wala")
                	setRunningBalance(0)              		
            	}            	

            })
  }, []);    


//SEARCH FUNCTION
  useEffect(() => {
        console.log("NAG SEARCH AKO O SEARCHTYPE")
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/search-record`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    searchType: searchType, 
                    searchKeyword: search     
                })
            })
            .then(res => res.json())
            .then(data => {
              setRecords(data)
              console.log(data)   
              if(records.length > 0){
              if(data.length > 1){
              setTransactTitle('Search Result: ' + data.length + ' Transactions')      
                }else{
              setTransactTitle('Search Result: ' + data.length + ' Transaction')      
                }
              }
                // const total = parseInt(data.reduce((prev,next) => prev + next.amount,0));
                // setAllIncome(total)
            })
  }, [search, searchType])

//SHOW ALL RECORDS
    function showAllRecords(e) {
        e.preventDefault(); 
        setRecords(allRecords)
        if(transactionsCount > 1){
        	//pluralTransaction
        	setTransactTitle('Showing All ' + transactionsCount + ' Transactions')        	
        }else{
        	//singular transaction
        	setTransactTitle('Showing All ' + transactionsCount + ' Transaction')       
           }
        window.scrollTo(0, 0)
        }

	return(
		<React.Fragment>
			<Head>
				<title>Transaction Records</title>
			</Head>
				
			<Container className="mt-5">
			{/* BOXES OVERVIEW */}
	 <div className="row">
            <div class="col-xl-4 col-md-4 mb-3">
              <div class="card border-left-primary bg-success shadow h-100 py-2 text-white">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-white text-uppercase mb-1">TOTAL INCOME</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><NumberFormat value={allIncome} displayType={'text'} thousandSeparator={true} prefix="&#8369; " /></div>
                    </div>
                    <div class="col-auto">
                       <FontAwesomeIcon icon={faHandHoldingUsd} color="white" size="3x"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div className="col-xl-4 col-md-4 mb-3">
              <div class="card border-left-success bg-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-white text-uppercase mb-1">TOTAL EXPENSE</div>
                      <div class="h5 mb-0 font-weight-bold text-white"><NumberFormat value={allExpense} displayType={'text'} thousandSeparator={true} prefix="&#8369; " /></div>
                    </div>
                    <div className="col-auto">
                   	 <FontAwesomeIcon icon={faMinusSquare} color="white" size="3x"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                
            <div className="col-xl-4 col-md-4 mb-3">
              <div class="card border-left-warning bg-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-white text-uppercase mb-1">AVAILABLE FUNDS</div>
                      <div class="h5 mb-0 font-weight-bold text-white"><NumberFormat value={runningBalance} displayType={'text'} thousandSeparator={true} prefix="&#8369; " /></div>
                    </div>
                    <div class="col-auto">
                     <FontAwesomeIcon icon={faWallet} color="white" size="3x"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
			{/* BOX DIV */}
      <Form inline>
			<Button href="newRecord" variant="primary" className="mr-2">Add Transaction</Button>
					<Button variant="secondary" className="mr-2">
					  Total Transactions <Badge variant="light">{transactionsCount}</Badge>
					</Button>			
          <Form.Control className="d-flex p-2 mr-2 my-2" style={{width: "600px"}} type="text" placeholder="Search transaction Here..." value={search} onChange={(e) => setSearch(e.target.value)}/>

                <Form.Control className="my-2" as="select" placeholder="Select Category Type" onChange={e => setSearchType(e.target.value)} custom required>
                      <option value="All">All</option>
                      <option value="Income">Income</option>
                      <option value="Expense">Expense</option>
                    </Form.Control>          
        </Form>
				<div className="my-3 p-3 bg-white rounded shadow-sm"> 
					{/* TITLE */}
					<h6 className="border-bottom border-gray pb-2 mb-0" >{transactTitle}</h6>
					{/* INCOME DIV */}

     				{records.map(record => {
                        		return(
									    <div className="media text-muted pt-3">
			   <span className={record.type == "Income" ? "btn btn-success mr-2" : "btn btn-danger mr-2"}>{record.type == "Income" ? "+" : "-"}</span>									   
									     	<div className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
									     		<div className="d-flex justify-content-between align-items-center w-100">
											          <strong className="text-gray-dark">{record.description} ({record.categoryName})</strong>
											          <h5 className={record.type == "Income" ? "text-success" : "text-danger"}>
											          {record.type == "Income" ? "+" : "-"}  <NumberFormat value={record.amount} displayType={'text'} thousandSeparator={true} prefix="&#8369; " /></h5>
					       						 </div>
					        					<div className="d-flex justify-content-between align-items-center w-100">
													   <span className="d-block">{moment(record.dateAdded).format('MMMM DD YYYY, h:mm a')}<h6><span class={record.type == "Income" ? "badge badge-success mt-2" : "badge badge-danger mt-2"}>{record.type}</span></h6></span>
					          						    <h4 className={record.type == "Income" ? "text-success" : "text-danger"}>
					          						    <NumberFormat value={record.balanceAfterTransaction} displayType={'text'} thousandSeparator={true}  /></h4>
					          					</div>
									     	</div>
									    </div>                        				
                        		)
                        	})} 
                        	{records.length > 0                					    
					    ? <small className="d-block text-right mt-3">
					      <a href="#" onClick={(e) => showAllRecords(e)}>Show All Records >></a>
					    </small>	
					    : <Alert className="mt-2" variant="danger">Transaction Not Existing!</Alert>
					}
						      
			{/* END OF BOX DIV */}					    
				</div>
			
				</Container>
		</React.Fragment>
	)
}