import { useState, useEffect } from 'react';
import { Form, Button, Container, InputGroup  } from 'react-bootstrap';
import Router from 'next/router';
import swal from 'sweetalert';
import AppHelper from '../app-helper'

export default function create() {
    //declare form input states
    const [typeName, setTypeName] = useState('');
    const [categoryName, setCategoryName] = useState('');
    const [amount, setAmount] = useState(0);
    const [description, setDescription] = useState('');
    const [categories, setCategory] = useState('');
    const [type, setType] = useState("Income");
    const [categoryData, setCategoryData] = useState([]);

    //function for processing creation of a new course
    // function createCourse(e) {
    //     e.preventDefault();

    //     console.log(`${courseName} with ID: ${courseId} is set to start on ${startDate} for PhP ${price} per slot.`);

    //     setCourseId('');
    //     setCourseName('');
    //     setDescription('');
    //     setPrice(0);
    //     setStartDate('');
    //     setEndDate('');
    // }


  useEffect(() => {
        console.log(localStorage.getItem('token'))
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-categories`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: type,      
                })
            })
            .then(res => res.json())
            .then(data => {
                setCategoryData(data)
            })
  }, [type]);




    //function to add  new Record
        function addRecord(e) {
            e.preventDefault();
            console.log(`Typename: ${type} categoryName: ${categoryName} amount: ${amount} description: ${description}`)

            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/add-record`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    typeName: type,
                    categoryName: categoryName,
                    amount: parseInt(amount),
                    description: description   
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data === true){
                     swal(`You add a new ${typeName}`, "View your new Record now!`", "success");
                    Router.push('/records')
                }else{
                    Router.push('/error')
                }
            })
        }



    return (
        <Container>
            <Form className="mt-5" onSubmit={(e) => addRecord(e)}>
                <h2 className="mb-3">New Record Transaction</h2>
                <Form.Group controlId="typeId">
                 <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" placeholder="Select Category Type" onChange={e => setType(e.target.value)} custom required>
                      <option  value="">Select Category Type</option>
                      <option value="Income">Income</option>
                      <option value="Expense">Expense</option>
                    </Form.Control>
                    </Form.Group>

                <Form.Group controlId="CategoryId">
                 <Form.Label>Select Catgories:</Form.Label>
                <Form.Control as="select" placeholder="Select Category Type" onChange={e => setCategoryName(e.target.value)} custom required>
                      <option  value="">Select Categories</option>
                            {categoryData.map(categories => {
                                return(
                                      <option key={categories._id} value={categories.name}>{categories.name} </option>
                                )
                            })}                        
                    </Form.Control>
                    </Form.Group>

                <Form.Group controlId="description">
                    <Form.Label>Amount</Form.Label>                
                    <Form.Control type="number" rows="3" placeholder="Enter Amount" value={amount} onChange={e => setAmount(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="price">
                    <Form.Label>Description</Form.Label>
                    <Form.Control as="textarea" value={description} onChange={e => setDescription(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit">Submit</Button>
            </Form>
        </Container>
    )
}
