import { useState, useEffect } from 'react'
import {Pie} from 'react-chartjs-2'
import Head from 'next/head'
import { Container } from 'react-bootstrap'
import { colorRandomizer } from '../helpers'

export default function PieChart() {
    const [brands, setBrands] = useState([])
    const [sales, setSales] = useState([])
    const [bgColors, setBgColors] = useState([])
    const [categories, setCategories] = useState([])
    const [amounts, setAmounts] = useState([])    

useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-records-breakdown-by-range`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    fromDate: "2020-10-20",   
                    toDate:   "2020-10-27"
                })
            })
        .then(res => res.json())
        .then(data => {
            if(data.length > 0){
                console.log(data)

                let arrCategories = []

                data.forEach(category => {
                    arrCategories.push(category.categoryName)
                    console.log(category.categoryName)
                 })

                console.log(arrCategories)
                setCategories(arrCategories)

                let arrAmounts = []

                data.forEach(category => {
                    arrAmounts.push(category.totalAmount)
                    console.log(category.totalAmount)
                 })

                console.log(arrAmounts)
                setAmounts(arrAmounts)   

                setBgColors(data.map(() => `#${colorRandomizer()}`))                             

            }
        })
    }, [])


    //prepare the data to be given to the react-chart-js-2 pie chart component
    const data = {
        labels: categories,
        datasets: [{
            data: amounts,
            backgroundColor: bgColors,
            hoverBackgroundColor: bgColors
        }]
    }

    //pass in the data to the pie chart component
    return (

		<React.Fragment>
			<Head>
				<title>Pie Chart</title>
			</Head>		
	<Container className="mt-5">  
	<h1>Category Breakdown</h1>  	
        <Pie data={data} />
        	</Container>
	    </React.Fragment>	
    )
}
