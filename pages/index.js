import Head from 'next/head'
import Banner from '../components/Banner'

export default function Home() {
  const data = {
    title: "RheyportHub",
    content: "Let's Monitor Your Business!"
  }
  return (
    <>
      <Head>
        <title>RheyportHub Budget & Inventory Tracker</title>
      </Head>
      <Banner data={data} />
    </>
  )
}