import { useState } from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router';
import swal from 'sweetalert';


export default function create() {
    //declare form input states
    const [name, setName] = useState('');
    const [type, setType] = useState("Income");
    //function for processing creation of a new course
    // function createCourse(e) {
    //     e.preventDefault();

    //     console.log(`${courseName} with ID: ${courseId} is set to start on ${startDate} for PhP ${price} per slot.`);

    //     setCourseId('');
    //     setCourseName('');
    //     setDescription('');
    //     setPrice(0);
    //     setStartDate('');
    //     setEndDate('');
    // }


    //function to add Category
        function addType(e) {
            e.preventDefault();
            console.log(`Name: ${name} Type: ${type}`)

            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/add-category`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: name,   
                    typeName: type     
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data === true){
                    swal("Category Added!", "You may now use your new Category", "success");
                    Router.push('/addCategory')

                    setName('')
                    setType('Income')
                    Router.push('/categories')
                }else{
                    Router.push('/error')
                }
            })
        }



    return (
        <Container className="mt-5">
            <Form onSubmit={(e) => addType(e)}>
                <Form.Group controlId="categoryName">
                    <Form.Label>Category Name:</Form.Label>
                    <Form.Control type="text" placeholder="Enter Name" value={name} onChange={e => setName(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="typeId">
                <Form.Control as="select" placeholder="Select Category Type" onChange={e => setType(e.target.value)} custom>
                      <option value="">Select Category Type</option>
                      <option value="Income">Income</option>
                      <option value="Expense">Expense</option>
                    </Form.Control>
                </Form.Group>

                <Button variant="primary" type="submit">Submit</Button>
            </Form>
        </Container>
    )
}
