import { useState, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import UserContext from '../UserContext'
import Router from 'next/router'
import Head from 'next/head'
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'
import AppHelper from '../app-helper'


export default function login() {
    //use the UserContext and destructure it to access the user and setUser defined in the App component
    const {User, setUser } = useContext(UserContext)

    //states for form input
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState(null)

//RETRIEVE USER DETAILS
    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data)
            setUser({ email: data.email, name: data.name})   
            Router.push('/records')
        })
    }


//GOOGLE FUNCTION AUTHENTICATION
    const authenticateGoogleToken = (response) => {
        console.log(response)
        setTokenId(response.tokenId)


        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }
        console.log("before google id token")
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, payload).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    console.log("auth error fire")
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    console.log("login type error fire")
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }


    //function to process user authentication
    function authenticate(e) {
        //prevent redirection via form submission
        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            //successful authentication will return a JWT via the response accessToken property
            if(data.accessToken){
                //store JWT in local storage
                localStorage.setItem('token', data.accessToken);
                //send a fetch request to decode JWT and obtain user ID and role for storing in context
                fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    } 
                })
                .then(res => res.json())
                .then(data => {
                    //set the global user state to have properties containing authenticated user's ID
                    setUser({
                        id: data._id
                    })
                    Router.push('/records')
                })
            }else{//authentication failure
                Router.push('/error')
            }
        })
    }

    return (
        <>
            <Head>
                <title>Login</title>
            </Head>
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
                    <h4 className="text-center">Login With Google</h4>
                    <GoogleLogin
                        clientId="837463427681-dmlbijshrqchc8a75egb5ucmckagng8a.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
        </>
    )
}