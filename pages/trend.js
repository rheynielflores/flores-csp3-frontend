import { useState, useEffect } from 'react'
import {Line} from 'react-chartjs-2'
import Head from 'next/head'
import { Container } from 'react-bootstrap'
import { colorRandomizer } from '../helpers'

export default function PieChart() {
    const [balTrend, setBalTrend] = useState([])
 

useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-most-recent-records`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                }
            })
        .then(res => res.json())
        .then(data => {
        	 console.log(data.reverse())
        	 let arrbalanceAfterTransaction = []

                data.forEach(bal => {
                    arrbalanceAfterTransaction.push(bal.balanceAfterTransaction)
                    console.log(bal.balanceAfterTransaction)
                 })
              console.log(arrbalanceAfterTransaction)
              setBalTrend(arrbalanceAfterTransaction)

        })
    }, [])


   // prepare the data to be given to the react-chart-js-2 pie chart component
    const data = {
    	labels: balTrend, 	
        datasets: [{
            data: balTrend,
            label: "Funds Movement",
            	      fill: false,
	      lineTension: 0.1,
	      backgroundColor: 'rgba(75,192,192,0.4)',
	      borderColor: 'rgba(75,192,192,1)',
	      borderCapStyle: 'butt',
	      borderDash: [],
	      borderDashOffset: 0.0,
	      borderJoinStyle: 'miter',
	      pointBorderColor: 'rgba(75,192,192,1)',
	      pointBackgroundColor: '#fff',
	      pointBorderWidth: 1,
	      pointHoverRadius: 5,
	      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
	      pointHoverBorderColor: 'rgba(220,220,220,1)',
	      pointHoverBorderWidth: 2,
	      pointRadius: 1,
	      pointHitRadius: 10,   

        }]
    }

    // pass in the data to the pie chart component

    return (

		<React.Fragment>
			<Head>
				<title>Balance Trend</title>
			</Head>		
	<Container className="mt-5">  
	<h1>Balance Trend</h1>  	
      <Line data={data} />
        	</Container>
	    </React.Fragment>	
    )
}
