import { useContext } from 'react'
//import necessary bootstrap components
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
//import nextJS Link component for client-side navigation
import Link from 'next/link'

import UserContext from '../UserContext'

export default function NavBar() {
    //consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext)

    return (
        <Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand">
                      <img src="https://i.imgur.com/fRLnEy5.png" alt=""/>
                </a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    
                    {(user.id !== null)
                        ? <>
                            <Link href="/records">
                                <a className="nav-link" role="button">My Wallet</a>
                            </Link>
                            <Link href="/categories">
                                <a className="nav-link" role="button">Categories</a>
                            </Link>
                            <Link href="/records">
                                <a className="nav-link" role="button">Records</a>
                            </Link>
                            <Link href="/typeInsight">
                                <a className="nav-link" role="button">Bar Chart</a>
                            </Link>                             
                            <Link href="/trend">
                                <a className="nav-link" role="button">Trends</a>
                            </Link>    
                            <Link href="/category-breakdown">
                                <a className="nav-link" role="button">BreakDown</a>
                            </Link>                                                      
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                        </>
                        : <>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
