import { useState, useEffect } from 'react'
import {Pie} from 'react-chartjs-2'
import { colorRandomizer } from '../helpers'

export default function PieChart() {
    const [brands, setBrands] = useState([])
    const [sales, setSales] = useState([])
    const [bgColors, setBgColors] = useState([])

    useEffect(() => {
        if(carData.length > 0){
            // segragate brands and sales into their respective states for visualization purposes
            // setBrands(carData.map(element => element.brand))
            // setSales(carData.map(element => element.sales))
            // randomize background colors for every section of our pie chart using our custom-made randomizer
            setBgColors(carData.map(() => `#${colorRandomizer()}`))
        }
    }, [carData])

    //prepare the data to be given to the react-chart-js-2 pie chart component
    const data = {
        labels: categories,
        datasets: [{
            data: amount,
            backgroundColor: bgColors,
            hoverBackgroundColor: bgColors
        }]
    }

    //pass in the data to the pie chart component
    return (
        <Pie data={data} />
    )
}
